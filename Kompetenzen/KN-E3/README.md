![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

[TOC]

# Kompetenznachweis E3


# Umsetzung und Review: (Einzelarbeit)

Code-Review zum **eigenen I3-Programm P1** durchgeführen und protokollieren. 
Laden Sie die [Vorlage](../../N2-Testing/Review_Vorlage_m319.docx) herunter und benutzen Sie diese!

## Anforderungen

	* Voraussetzung: Programm P1 entspricht der "Quelltext Konvention TBZ-IT". [siehe N3 Code Formatting](../../N3-Code_Formatting)
* Umsetzung und Abnahme des eigenen Projekts I3 (P1) erfolgt. <br> *KN-E3 kann vorzugsweise mit der Abnahme von KN-I3 kombiniert werden!*
* Vorlage Code-Review verwenden!



## Vorgehen

1. Platzieren Sie die *kopierte* Planer-Karte vom KN-Feld E3 in ihren Planer "**In progress**".
2. Den Code-Review vorbereiten. (Siehe Kap. 1 bis 2.1 [Vorlage](../../N2-Testing/Review_Vorlage_m319.docx))
3. Informieren Sie die LP und geben Sie das Protokoll ab (Planer-Karte auf "**To examine**" mit Link in der Planer-Karte &#8594; Portfolio).
4. Die Lehrperson führt als "Experte" durch den Code-Review-Prozess und protokolliert die Erkenntnisse im Bericht.
5. Die Lehrperson platziert die entsprechende Planer-Karte im Planer auf "**Passed**" oder "**Redo**" &#8594; 4.








